import imaplib
import ssl
import email
import requests
import sqlite3
import time

# Your account
username = "sample@gmail.com" # Set username here
password = "samplepassword" # Set password here
url_bot = "https://discord.com/api/webhooks/SAMPLE_STUFF"  # Set the URL_BOT webhook here.

# Lets connect to the IMAP server... THIS USES GMAIL, CAN BE CHANGED LIKE SO:
# https://www.arclab.com/en/kb/email/list-of-smtp-and-imap-servers-mailserver-list.html
server = "imap.gmail.com"
port = 993

# If anything goes wrong, shoves an error in attempting to connect to gmail.
try:
    mail = imaplib.IMAP4_SSL(server, port, ssl_context=ssl.create_default_context())
except:
    print("Failed to connect to the IMAP4_SSL specified server.")

# Authenticate
mail.login(username, password)

# Taking some emails
status, messages = mail.select("INBOX")

# Searching through the emails in the inbox.
data = mail.search(None, 'ALL')
mail_ids = data[1]  # Selects the next array element of data
id_list = mail_ids[0].split() # Splits the string into an array to use.

# Taking the first and last email id.
first_email_id = int(id_list[0])
latest_email_id = int(id_list[-1])

for id in reversed(id_list[-20:]):
    data_mail = mail.fetch(id, '(RFC822)')  # str(i) is the email id

    parser = email.parser.BytesParser()  # We want to interpret the gibberish spouted

    parsed_mail = parser.parsebytes(
        data_mail[1][0][1])  # Putting into the parser a specific header located in [1][0][1]

    from_mail = parsed_mail["From"]  # Retrieves from protected attribute the header start "from"
    subject_mail = parsed_mail["Subject"]  #
    date_mail = parsed_mail["Date"]  #
    to_mail = parsed_mail["To"]  #

    bot_send_bool = True  # If we need the bot to send, initially set to true.

    # TO CREATE THE DATABASE FOR COMPARISON
    database_comparison = sqlite3.connect('mailies.db')  # The databse name
    database_comparison_cursor = database_comparison.cursor()  # The cursor to edit db
    database_comparison_cursor.execute(
        ''' CREATE TABLE IF NOT EXISTS mail_seen (email_received TEXT, subject TEXT, dated TEXT, email_to TEXT) ''')  # Creates table for the DB. If it exists, does nothing.

    database_comparison_cursor.execute("SELECT 1 FROM mail_seen WHERE email_received = ? AND subject = ? AND dated = ? AND email_to = ?", (from_mail, subject_mail, date_mail, to_mail)) # This will ask the database to look up these 4 subjects and see if it gets a value back. If it does, it is in the database and it is sent already.
    if database_comparison_cursor.fetchone() is not None:  # If above does not exist, sent to the database
        bot_send_bool = False
    else:  # Else it remains as true and inserts vals into the database if if did not exist in the first place.
        database_comparison_cursor.execute(f"INSERT INTO mail_seen VALUES (?, ?, ?, ?)", (
        from_mail, subject_mail, date_mail, to_mail))  # Inserts the values into the DB

    database_comparison.commit()
    database_comparison.close()
    # END DATA BASE CREATION FOR COMPARISON

    if bot_send_bool == True:
        composed_alert = [from_mail,  # The fully composed email.
                          subject_mail,
                          date_mail,
                          to_mail]

        discord_contents = \
            {
                "username": from_mail[0:31],  # Can be removed to only give the webhook name.
                "embeds": [{"title": f"Mail Received titled {subject_mail}. \n",
                            "description": f"Received: {from_mail}, \n"
                                           f"Dated: {date_mail}, \n"
                                           f"To: {to_mail}"
                            }]
            }  # Essentially the contents that will be sent to the discbot. Taken from https://discord.com/developers/docs/resources/channel#embed-object


        requests.post(url_bot, json=discord_contents, headers={"Content-Type": "application/json"})

        time.sleep(3) # Should stop discord from thinking that I am spamming.
